﻿using System;
using System.Net;
using System.IO;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Win32;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Excel = Microsoft.Office.Interop.Excel;

namespace UBI_Lab2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static string nameLocationDB = null; // Путь до базы данных на компьюторе
        public static ObservableCollection<Dangers> collectionLocDB = null; // Коллекция для хранения локальной базы данных
        public static ObservableCollection<Dangers> collectionLocBeforeDB = null; // Коллекция для хранения предыдущей информации
        public static ObservableCollection<UpdateBeforeAfter> collectionDG = null; // Коллекция для хранения изменений 
        static int Row = 0; // Переменная необходимая для хранения номера строки начала страницы
        public static int StatusUpdate = -1; // Переменная необходимая для хранения номера строки начала страницы -1 = Ошибка при проверки обновлении, 1 = Нет обновления, 2 = Есть обновление
        public static int CountNotesUpdate; // Кол-во обновленных строк
        public MainWindow()
        {
            InitializeComponent();
            try // Пытаемся найти локальную базу данных
            {
                nameLocationDB = Directory.GetFiles(Directory.GetCurrentDirectory(), "LocBD.xlsx")[0].ToString();
            }
            catch // Если не нашли локальную базу данных, то выводи сообщение:
            {
                MessageBox.Show("Файла с локальной базой не существует!\n" +
                "Перед началом работы требуется первичная загрузка данных.\n"
                + "Для этого нажмите кнопку \"Создать локальную базу\".");
            }
            if(nameLocationDB != null)
            {
                ReadExcel(nameLocationDB); // Считываем локальную базу данных из Excel
                PrintStringsDG(dgMain); // Выводим информацию в datagrid
                CheckUpdateBD(LabelStatus, DateLabelUpdate); // Проверка обновления
                if(StatusUpdate == 1)
                {
                    MessageBox.Show("У Вас последняя версия базы данных!");
                }
                if (StatusUpdate == 2)
                {
                    MessageBox.Show("Ваша локальная база устарела!\n" +
                    "Обновите локальную базу данных!\n" +
                    "Для это нажмите кнопку: \"Обновить базу данных\"");
                }
            }
        }
        public static void ReadExcel(string nameFile) // Метод считывающий Excel файл
        {
            collectionLocDB = new ObservableCollection<Dangers>();

            Excel.Application ExcApp; 
            Excel.Workbook ExcWorkBook;
            Excel.Worksheet ExcWorkSheet;
            Excel.Range ExcRange;

            ExcApp = new Excel.Application();
            ExcWorkBook = ExcApp.Workbooks.Open(nameFile);
            ExcWorkSheet = ExcWorkBook.Worksheets["Sheet"];
            ExcRange = ExcWorkSheet.UsedRange;

            for (int i = 3; i <= ExcRange.Rows.Count; i++)
            {
                collectionLocDB.Add(new Dangers(ExcRange.Cells[i, 1].Text, ExcRange.Cells[i, 2].Text, ExcRange.Cells[i, 3].Text, ExcRange.Cells[i, 4].Text,
                    ExcRange.Cells[i, 5].Text, ExcRange.Cells[i, 6].Text, ExcRange.Cells[i, 7].Text, ExcRange.Cells[i, 8].Text, ExcRange.Cells[i, 9].Text,
                    ExcRange.Cells[i, 10].Text));
            }
            ExcWorkBook.Close();
            ExcApp.Quit();
        }
        public static void PrintStringsDG(DataGrid dgMain) // Метод, который выводит информацию в datagrid
        {
            ObservableCollection<Dangers> temp = new ObservableCollection<Dangers>(); // Создаем темповую коллекцию
            int predel; // Предельное значение
            if (Row + 15 < collectionLocDB.Count) // Если кол-во строк не привышает максимальное значение
            {
                predel = Row + 15;
            }
            else // Если привышает 
            {
                predel = collectionLocDB.Count;
            }
            for (int i = Row; i < predel; i++) // Заполняем темповую коллекцию значениями
            {
                temp.Add(collectionLocDB[i]);
            }
            dgMain.ItemsSource = null; // "Стираем" предыдущие значения в datagrid
            dgMain.ItemsSource = temp; // Заполняем новыми значениями datagrid
        }
        public static void WebServisDown(string fileName) // Метод, который скачивает Excel-файл и сохраняет
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("https://bdu.fstec.ru/files/documents/thrlist.xlsx");
            req.Credentials = CredentialCache.DefaultCredentials;
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            WebResponse respon = req.GetResponse();
            using (Stream res = respon.GetResponseStream())
            {
                using (FileStream Locfile = new FileStream(fileName, FileMode.Create))
                {
                    res.CopyTo(Locfile);
                }
            }
        }

        public static bool FileCompare(string file1, string file2) // Метод, который сравнивает два файла между собой
        {
            if (file1 == file2) // Если имена файлов совпадают, то она равны
            {
                return true;
            }

            int file1byte; // Хранилища байтов для первого файла
            int file2byte; // Хранилища байтов для второго файла
            FileStream fs1; // Поток для первого файла
            FileStream fs2; // Поток для второго файла

            fs1 = new FileStream(file1, FileMode.Open, FileAccess.Read); // "Открывем" поток для первого файла на чтение
            fs2 = new FileStream(file2, FileMode.Open, FileAccess.Read); // "Открывем" поток для второго файла на чтение

            if (fs1.Length != fs2.Length) // Если их длина различается, то они различны
            {
                fs1.Close();
                fs2.Close();

                return false;
            }

            do // Считываем байты с файлов
            {
                file1byte = fs1.ReadByte();
                file2byte = fs2.ReadByte();
            }
            while ((file1byte == file2byte) && (file1byte != -1));

            fs1.Close(); // Закрываем потоки
            fs2.Close();

            return (file1byte - file2byte) == 0; // Если массивы байтов различны то и файлы различны
        }

        public static void CheckUpdateBD(Label LabelStatus, Label DateLabelUpdate) // Метод проверяющий обновление
        {
            string nameOnlineDB = null; // Путь хранения файла онлайн БД
            File.Delete("OnlineBD.xlsx"); // Удаляем старую версию онлайн БД
            try // Пытаемся создать новую версию 
            {
                WebServisDown("OnlineBD.xlsx");
            }
            catch (Exception excep) // Если Web-ресур не доступен
            {
                MessageBox.Show(excep.Message + "\n" +
                "Не получается проверить обновление!\n" +
                "Web-ресур не доступен в данный момент времени!\n" +
                "Пути решения:\n" +
                "1) Проверить интернет соединение;\n" +
                "2) Или в данный момент нет доступа к Web-странице.");
            }
            try // Пытаемся найти онлайн базу данных
            {
                nameOnlineDB = Directory.GetFiles(Directory.GetCurrentDirectory(), "OnlineBD.xlsx")[0].ToString();
            }
            catch { }
            if(nameOnlineDB != null) // Если все прошло успешно
            {
                if(FileCompare("LocBD.xlsx", "OnlineBD.xlsx")) // Сравниваем два файла
                {
                    StatusUpdate = 1; // Если файлы одинаковые, то обновление не требуется
                    LabelStatus.Content = "Статус: Нет обновления";

                } else
                {
                    StatusUpdate = 2; // Если они различны
                    LabelStatus.Content = "Статус: Есть обновление";
                }
            } else
            {
                StatusUpdate = -1; // Если не получилось проверить обновление ДБ
                LabelStatus.Content = "Статус: Ошибка!";
            }
            DateLabelUpdate.Content = "Время последней проверки обновления: " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss");
            File.Delete("OnlineBD.xlsx"); // Удаляем старую версию онлайн БД
        }

        private void GetOnlineBD_Click(object sender, RoutedEventArgs e) // Нажатие на кнопку "Создать локальную базу"
        {
            if (nameLocationDB == null) // Если локальной базы еще не существует
            {
                try // Пытаем создать локальную базу данных
                {
                    WebServisDown("LocBD.xlsx");
                } catch(Exception excep) // Если Web-ресур не доступен
                {
                    MessageBox.Show(excep.Message + "\n" +
                        "Web-ресур не доступен в данный момент времени!\n" +
                        "Пути решения:\n" +
                        "1) Проверить интернет соединение;\n" +
                        "2) Или в данный момент нет доступа к Web-странице.");
                }
                try // Пытаемся найти локальную базу данных
                {
                    nameLocationDB = Directory.GetFiles(Directory.GetCurrentDirectory(), "LocBD.xlsx")[0].ToString();
                }
                catch { }
                if(nameLocationDB != null) // Если получилось загрузить файл
                {
                    ReadExcel(nameLocationDB); // Считываем локальную базу данных 
                    PrintStringsDG(dgMain); // Вывод БД в datagrid
                    CheckUpdateBD(LabelStatus, DateLabelUpdate); // Проверка обновления
                }
            }
            else // Если локальная база уже существует, то выводим путь к ней пользователю
            {
                MessageBox.Show("Локальная база уже существует!\n"
                    + "Путь к локальной базе на компьюторе:\n"
                    + nameLocationDB);
            }
        }

        private void NextPage_Click(object sender, RoutedEventArgs e) // Нажатие на кнопку "Следующая страница"
        {
            if (collectionLocDB != null) // Если коллекция не пустая
            {
                if (Row + 15 < collectionLocDB.Count) // Если размер коллекции позволяет выввести следующие 15 строк
                {
                    Row += 15;
                    PrintStringsDG(dgMain);
                }
                else if (Row < collectionLocDB.Count) // Если осталось меньше 15 строк в коллекции
                {
                    PrintStringsDG(dgMain);
                }
            }
        }

        private void PreviousPage_Click(object sender, RoutedEventArgs e) // Нажатие на кнопку "Предыдущая страница"
        {
            if (collectionLocDB != null) // Если коллекция не пустая
            {
                if (Row - 15 > 0) // Если размер коллекции позволяет выввести следующие 15 строк
                {
                    Row -= 15;
                    PrintStringsDG(dgMain);
                }
                else if (Row > 0) // Если осталось меньше 15 строк в коллекции
                {
                    Row = 0;
                    PrintStringsDG(dgMain);
                }
            }
        }

        private void CheckUpdate_Click(object sender, RoutedEventArgs e) // Проверка обновления при нажатии на кнопку "Проверить обновление"
        {
            if (collectionLocDB != null)
            {
                CheckUpdateBD(LabelStatus, DateLabelUpdate);
                if (StatusUpdate == 2)
                {
                    MessageBox.Show("Ваша локальная база устарела!\n" +
                    "Обновите локальную базу данных!\n" +
                    "Для это нажмите кнопку: \"Обновить базу данных\"");
                }
            }
        }
        private void UpdateDB_Click(object sender, RoutedEventArgs e)
        {
            if (collectionLocDB != null) // Есть локальная БД
            {
                CheckUpdateBD(LabelStatus, DateLabelUpdate); // Проверяем статус обновления
                if(StatusUpdate == 2) // Если есть обновление
                {
                    WebServisDown("LocBD.xlsx"); // Обновление локальной базы
                    collectionLocBeforeDB = collectionLocDB; // Сохранение предыдущих записей
                    ReadExcel(nameLocationDB); // Чтение Excel файла (обновленный)
                    collectionDG = new ObservableCollection<UpdateBeforeAfter>(); // Коллекция для сравнения
                    CountNotesUpdate = 0; // Кол-во измененных записей
                    for (int i = 0; i < collectionLocBeforeDB.Count; i++)
                    {
                        string NumberUBI = null;
                        string Before = "";
                        string After = "";
                        if (collectionLocBeforeDB[i].NumberUBI != collectionLocDB[i].NumberUBI) // Если изменился номер УБИ
                        {
                            if (NumberUBI == null)
                            {
                                NumberUBI = collectionLocDB[i].NumberUBI;
                            }
                            Before += "Идентификатор УБИ: " + collectionLocBeforeDB[i].NumberUBI + "\n";
                            After += "Идентификатор УБИ: " + collectionLocDB[i].NumberUBI + "\n";
                        }
                        if (collectionLocBeforeDB[i].NameUBI != collectionLocDB[i].NameUBI) // Если изменилось наименование
                        {
                            if (NumberUBI == null)
                            {
                                NumberUBI = collectionLocDB[i].NumberUBI;
                            }
                            Before += "Наименование УБИ: " + collectionLocBeforeDB[i].NameUBI + "\n";
                            After += "Наименование УБИ: " + collectionLocDB[i].NameUBI + "\n";
                        }
                        if (collectionLocBeforeDB[i].Description != collectionLocDB[i].Description) // Если изменилось описание
                        {
                            if (NumberUBI == null)
                            {
                                NumberUBI = collectionLocDB[i].NumberUBI;
                            }
                            Before += "Описание: " + collectionLocBeforeDB[i].Description + "\n";
                            After += "Описание: " + collectionLocDB[i].Description + "\n";
                        }
                        if (collectionLocBeforeDB[i].Source != collectionLocDB[i].Source) // Если изменился источник угрозы
                        {
                            if (NumberUBI == null)
                            {
                                NumberUBI = collectionLocDB[i].NumberUBI;
                            }
                            Before += "Источник угрозы: " + collectionLocBeforeDB[i].Source + "\n";
                            After += "Источник угрозы: " + collectionLocDB[i].Source + "\n";
                        }
                        if (collectionLocBeforeDB[i].ObjectImpact != collectionLocDB[i].ObjectImpact) // Если изменился объект воздействия
                        {
                            if (NumberUBI == null)
                            {
                                NumberUBI = collectionLocDB[i].NumberUBI;
                            }
                            Before += "Объект воздействия: " + collectionLocBeforeDB[i].ObjectImpact + "\n";
                            After += "Объект воздействия: " + collectionLocDB[i].ObjectImpact + "\n";
                        }
                        if (collectionLocBeforeDB[i].ViolationConf != collectionLocDB[i].ViolationConf) // Если изменилось: Нарушение конфиденциальности
                        {
                            if (NumberUBI == null)
                            {
                                NumberUBI = collectionLocDB[i].NumberUBI;
                            }
                            Before += "Нарушение конфиденциальности: " + collectionLocBeforeDB[i].ViolationConf + "\n";
                            After += "Нарушение конфиденциальности: " + collectionLocDB[i].ViolationConf + "\n";
                        }
                        if (collectionLocBeforeDB[i].ViolationIntegrities != collectionLocDB[i].ViolationIntegrities) // Если изменилось: Нарушение целостности
                        {
                            if (NumberUBI == null)
                            {
                                NumberUBI = collectionLocDB[i].NumberUBI;
                            }
                            Before += "Нарушение целостности: " + collectionLocBeforeDB[i].ViolationIntegrities + "\n";
                            After += "Нарушение целостности: " + collectionLocDB[i].ViolationIntegrities + "\n";
                        }
                        if (collectionLocBeforeDB[i].ViolationAvailability != collectionLocDB[i].ViolationAvailability)// Если изменилось: Нарушение доступности
                        {
                            if (NumberUBI == null)
                            {
                                NumberUBI = collectionLocDB[i].NumberUBI;
                            }
                            Before += "Нарушение доступности: " + collectionLocBeforeDB[i].ViolationAvailability + "\n";
                            After += "Нарушение доступности: " + collectionLocDB[i].ViolationAvailability + "\n";
                        }
                        if (collectionLocBeforeDB[i].ActivationDate != collectionLocDB[i].ActivationDate)// Если изменилось: Дата включения угрозы в БДУ
                        {
                            if (NumberUBI == null)
                            {
                                NumberUBI = collectionLocDB[i].NumberUBI;
                            }
                            Before += "Дата включения угрозы в БДУ: " + collectionLocBeforeDB[i].ActivationDate + "\n";
                            After += "Дата включения угрозы в БДУ: " + collectionLocDB[i].ActivationDate + "\n";
                        }
                        if (collectionLocBeforeDB[i].DateLastMod != collectionLocDB[i].DateLastMod)// Если изменилось: Дата последнего изменения данных
                        {
                            if (NumberUBI == null)
                            {
                                NumberUBI = collectionLocDB[i].NumberUBI;
                            }
                            Before += "Дата последнего изменения данных: " + collectionLocBeforeDB[i].DateLastMod + "\n";
                            After += "Дата последнего изменения данных: " + collectionLocDB[i].DateLastMod + "\n";
                        }
                        if (NumberUBI != null) // Если запись изменилась
                        {
                            collectionDG.Add(new UpdateBeforeAfter(NumberUBI, Before, After));
                            CountNotesUpdate++;
                        }
                    }
                    if (collectionLocBeforeDB.Count < collectionLocDB.Count) // Если появились новые записи
                    {
                        for (int i = collectionLocBeforeDB.Count; i < collectionLocDB.Count; i++)
                        {
                            string Before = "Новая запись!";
                            string After = "";
                            After += "Идентификатор УБИ: " + collectionLocDB[i].NumberUBI + "\n";
                            After += "Наименование УБИ: " + collectionLocDB[i].NameUBI + "\n";
                            After += "Описание: " + collectionLocDB[i].Description + "\n";
                            After += "Источник угрозы: " + collectionLocDB[i].Source + "\n";
                            After += "Объект воздействия: " + collectionLocDB[i].ObjectImpact + "\n";
                            After += "Нарушение конфиденциальности: " + collectionLocDB[i].ViolationConf + "\n";
                            After += "Нарушение целостности: " + collectionLocDB[i].ViolationIntegrities + "\n";
                            After += "Нарушение доступности: " + collectionLocDB[i].ViolationAvailability + "\n";
                            After += "Дата включения угрозы в БДУ: " + collectionLocDB[i].ActivationDate + "\n";
                            After += "Дата последнего изменения данных: " + collectionLocDB[i].DateLastMod + "\n";
                            collectionDG.Add(new UpdateBeforeAfter(collectionLocDB[i].NumberUBI, Before, After));
                            CountNotesUpdate++;
                        }
                    }
                    Row = 0;
                    PrintStringsDG(dgMain); // Обновление записей в datagrid
                    CheckUpdateBD(LabelStatus, DateLabelUpdate);
                    new Update().ShowDialog(); // Вывод окна с изменениями
                } else if (StatusUpdate == 1)
                {
                    MessageBox.Show("У Вас последняя версия базы данных!");
                }
            }
        }

        private void OpenBriefInf_Click(object sender, RoutedEventArgs e) // Вывод БД в краткой форме
        {
            if(collectionLocDB != null) // Если существует локальная БД
            {
                new BriefInformation().ShowDialog();
            } else
            {
                MessageBox.Show("Файла с локальной базой не существует!\n" +
                "Перед началом работы требуется первичная загрузка данных.\n" +
                "Для этого нажмите кнопку \"Создать локальную базу\".");
            }
        }
    }
}
