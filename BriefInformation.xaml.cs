﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UBI_Lab2
{
    /// <summary>
    /// Логика взаимодействия для BriefInformation.xaml
    /// </summary>
    public partial class BriefInformation : Window
    {
        static int Row = 0;
        public BriefInformation()
        {
            InitializeComponent();
            PrintBriefInf(dgBriedInf); // Вывод первой стр.
        }

        public static void PrintBriefInf(DataGrid dataGrid) // Метод, который выводит информацию в datagrid
        {
            ObservableCollection<Dangers> temp = new ObservableCollection<Dangers>();
            int predel;
            if (Row + 15 < MainWindow.collectionLocDB.Count)
            {
                predel = Row + 15;
            }
            else
            {
                predel = MainWindow.collectionLocDB.Count;
            }
            for (int i = Row; i < predel; i++)
            {
                temp.Add(new Dangers(MainWindow.collectionLocDB[i].NumberUBI, MainWindow.collectionLocDB[i].NameUBI));
            }
            dataGrid.ItemsSource = null;
            dataGrid.ItemsSource = temp;
        }
        private void PreviousPage_Click(object sender, RoutedEventArgs e) // На предыдущую страницу 
        {
            if (Row - 15 > 0)
            {
                Row -= 15;
                PrintBriefInf(dgBriedInf);
            }
            else if (Row > 0)
            {
                Row = 0;
                PrintBriefInf(dgBriedInf);
            }
        }

        private void NextPage_Click(object sender, RoutedEventArgs e) // На след. стр.
        {
            if (Row + 15 < MainWindow.collectionLocDB.Count)
            {
                Row += 15;
                PrintBriefInf(dgBriedInf);
            }
            else if (Row < MainWindow.collectionLocDB.Count)
            {
                PrintBriefInf(dgBriedInf);
            }
        }
    }
}
