﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBI_Lab2
{
    public class UpdateBeforeAfter
    {
        public string NumberUBI { get; set; }
        public string Before { get; set; }
        public string After { get; set; }
        public UpdateBeforeAfter(string NumberUBI, string Before, string After)
        {
            this.NumberUBI = NumberUBI;
            this.Before = Before;
            this.After = After;
        }
    }
}
