﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBI_Lab2
{
    public class Dangers
    {
        public string NumberUBI { get; set; }
        public string NameUBI { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string ObjectImpact { get; set; }
        public string ViolationConf { get; set; }
        public string ViolationIntegrities { get; set; }
        public string ViolationAvailability { get; set; }
        public string ActivationDate { get; set; }
        public string DateLastMod { get; set; }
        public Dangers(string NumberUBI, string NameUBI, string Description, string Source, string ObjectImpact, string ViolationConf, string ViolationIntegrities,
            string ViolationAvailability, string ActivationDate, string DateLastMod)
        {
            this.NumberUBI = NumberUBI;
            this.NameUBI = NameUBI;
            this.Description = Description;
            this.Source = Source;
            this.ObjectImpact = ObjectImpact;
            if (ViolationConf == "1")
            {
                this.ViolationConf = "да";
            }
            else
            {
                this.ViolationConf = "нет";
            }
            if (ViolationIntegrities == "1")
            {
                this.ViolationIntegrities = "да";
            }
            else
            {
                this.ViolationIntegrities = "нет";
            }
            if (ViolationAvailability == "1")
            {
                this.ViolationAvailability = "да";
            }
            else
            {
                this.ViolationAvailability = "нет";
            }
            this.ActivationDate = ActivationDate;
            this.DateLastMod = DateLastMod;
        }
        public Dangers(string NumberUBI, string NameUBI)
        {
            if (int.Parse(NumberUBI) < 10)
            {
                this.NumberUBI = "УБИ.00" + NumberUBI;
            }
            else if (int.Parse(NumberUBI) < 100)
            {
                this.NumberUBI = "УБИ.0" + NumberUBI;
            }
            else
            {
                this.NumberUBI = "УБИ." + NumberUBI;
            }
            this.NameUBI = NameUBI;
        }
    }
}
