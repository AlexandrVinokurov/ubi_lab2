﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UBI_Lab2
{
    /// <summary>
    /// Логика взаимодействия для Update.xaml
    /// </summary>
    public partial class Update : Window
    {
        public Update()
        {
            InitializeComponent();
            StatusUpdate.Content = "Статуса обновления: Успешно";
            CountNoteUpdate.Content = "Количество обновленных записей: " + MainWindow.CountNotesUpdate;
            UpdateDG.ItemsSource = null;
            UpdateDG.ItemsSource = MainWindow.collectionDG;
        }
    }
}
